package simplifier

import AST._
import scala.collection.mutable.ListBuffer
import scala.math.pow

// to implement
// avoid one huge match of cases
// take into account non-greedy strategies to resolve cases with power laws

object Simplifier {

  def simplify(node: Node):Node = node match {

    case KeyDatumList(list) => {
      var dict = Map(list(0).key -> list(0).value)
      list.map(x => dict += x.key -> x.value)
      KeyDatumList(list.filter(x => dict(x.key) == x.value))
    }

    case Unary("not", BinExpr("==", Variable(x),Variable(y))) => BinExpr("!=", Variable(x),Variable(y))
    case Unary("not", BinExpr("!=", Variable(x),Variable(y))) => BinExpr("==", Variable(x),Variable(y))
    case Unary("not", BinExpr("<=", Variable(x),Variable(y))) => BinExpr(">", Variable(x),Variable(y))
    case Unary("not", BinExpr(">=", Variable(x),Variable(y))) => BinExpr("<", Variable(x),Variable(y))
    case Unary("not", BinExpr(">", Variable(x),Variable(y))) => BinExpr("<=", Variable(x),Variable(y))
    case Unary("not", BinExpr("<", Variable(x),Variable(y))) => BinExpr(">=", Variable(x),Variable(y))
    case Unary("not", TrueConst()) => FalseConst()
    case Unary("not", FalseConst()) => TrueConst()

    case Unary("not", Unary("not", value)) => simplify(value)
    case Unary("-", Unary("-", value)) => simplify(value)
    case BinExpr(op, FloatNum(a), FloatNum(b)) => {
      op match {
        case "*" => FloatNum(a*b)
        case "**" => FloatNum(pow(a,b))
        case "/" => FloatNum(a/b)
        case "+" => FloatNum(a+b)
        case "-" => FloatNum(a-b)
        case "%" => FloatNum(a%b)
        case ">" => if (a > b) TrueConst() else FalseConst()
        case "<" => if (a < b) TrueConst() else FalseConst()
        case "==" => if (a == b) TrueConst() else FalseConst()
        case ">=" => if (a >= b) TrueConst() else FalseConst()
        case "<=" => if (a <= b) TrueConst() else FalseConst()
      }
    }
    case BinExpr(op, IntNum(a), IntNum(b)) => {
      op match {
        case "*" => IntNum(a*b)
        case "**" => IntNum(pow(a.toDouble,b.toDouble).toInt)
        case "/" => IntNum(a/b)
        case "+" => IntNum(a+b)
        case "-" => IntNum(a-b)
        case "%" => IntNum(a%b)
        case ">" => if (a > b) TrueConst() else FalseConst()
        case "<" => if (a < b) TrueConst() else FalseConst()
        case "==" => if (a == b) TrueConst() else FalseConst()
        case ">=" => if (a >= b) TrueConst() else FalseConst()
        case "<=" => if (a <= b) TrueConst() else FalseConst()
      }
    }



    case BinExpr("-", Variable(x), Variable(y)) => if (x == y) IntNum(0) else BinExpr("-", Variable(x), Variable(y))
    case BinExpr("-",BinExpr("+",a,b),c) => if(a == c) b else if(b == c) a else BinExpr("-",simplify(BinExpr("+",a,b)),c)
    case BinExpr("-",BinExpr("*",IntNum(a),x), y) => if(x == y) simplify(BinExpr("*",IntNum(a-1),x)) else BinExpr("-",simplify(BinExpr("*",IntNum(a),x)), y)
    case BinExpr("+",BinExpr("*",IntNum(a),x), y) => if(x == y) simplify(BinExpr("*",IntNum(a+1),x)) else BinExpr("-",simplify(BinExpr("*",IntNum(a),x)), y)
    case BinExpr("-",BinExpr("*",FloatNum(a),x), y) => if(x == y) simplify(BinExpr("*",FloatNum(a-1),x)) else BinExpr("-",simplify(BinExpr("*",FloatNum(a),x)), y)
    case BinExpr("+",BinExpr("*",FloatNum(a),x), y) => if(x == y) simplify(BinExpr("*",FloatNum(a+1),x)) else BinExpr("-",simplify(BinExpr("*",FloatNum(a),x)), y)
    case BinExpr("+", IntNum(a), Variable(x)) => if (a == 0) Variable(x) else BinExpr("+", IntNum(a), Variable(x))
    case BinExpr("+", BinExpr("+", BinExpr("+", BinExpr("*", Variable(a), Variable(b)), BinExpr("*", Variable(c), Variable(d))), BinExpr("*", Variable(e), Variable(f))), BinExpr("*",Variable(g), Variable(h))) => if (a == c && b == f && d == h && e == g) {
      BinExpr("*", BinExpr("+", Variable(a), Variable(e)), BinExpr("+", Variable(b), Variable(d)))
    } else node

    case BinExpr("+", Variable(x), IntNum(a)) => if (a == 0) Variable(x) else BinExpr("+", Variable(x), IntNum(a))
    case BinExpr("*", IntNum(a), b) => if (a == 1) b else if (a == 0) IntNum(0) else BinExpr("*", IntNum(a), b)
    case BinExpr("*", b, IntNum(a)) => if (a == 1) b else if (a == 0) IntNum(0) else BinExpr("*", b, IntNum(a))
    case BinExpr("+", FloatNum(a), Variable(x)) => if (a == 0.0) Variable(x) else BinExpr("+", FloatNum(a), Variable(x))

    case BinExpr("+", Variable(x), FloatNum(a)) => if (a == 0.0) Variable(x) else BinExpr("+", Variable(x), FloatNum(a))
    case BinExpr("*", FloatNum(a), b) => if (a == 1.0) b else if (a == 0.0) FloatNum(0) else BinExpr("*", FloatNum(a), b)
    case BinExpr("*", b, FloatNum(a)) => if (a == 1.0) b else if (a == 0.0) FloatNum(0) else BinExpr("*", b, FloatNum(a))
    case BinExpr("+", BinExpr("*", a,b), BinExpr("*", c, d)) => if(a == c) simplify(BinExpr("*",a,BinExpr("+", b,d))) else if (a == d) simplify(BinExpr("*",a,BinExpr("+", b ,c))) else if (b == c) simplify(BinExpr("*",b,BinExpr("+", a,d))) else if (b == d)simplify(BinExpr("*",b,BinExpr("+", a,c))) else BinExpr("+", simplify(BinExpr("*", a,b)), simplify(BinExpr("*", c, d)))

    //Power Laws
    case BinExpr("*", BinExpr("**", a, y), BinExpr("**", b, z)) => if(a == b)  BinExpr("**", a, BinExpr("+", y, z)) else BinExpr("**", simplify(BinExpr("**", a, y)), simplify(BinExpr("**", b, z)))
    case BinExpr("**", BinExpr("**", x, Variable(y)), Variable(z)) => BinExpr("**", x, BinExpr("*", Variable(y), Variable(z)))
    case BinExpr("**", BinExpr("**", x, IntNum(y)), IntNum(z)) => simplify(BinExpr("**", x, simplify(BinExpr("**", IntNum(y), IntNum(z)))))
//    a**0 = 1 and a**1 = a
    case BinExpr("**", Variable(a), FloatNum(x)) => if (x == 0.0) FloatNum(1) else if (x == 1.0) Variable(a) else BinExpr("**", Variable(a), FloatNum(x))
    case BinExpr("**", Variable(a), IntNum(x)) => if (x == 0) IntNum(1) else if (x == 1) Variable(a) else BinExpr("**", Variable(a), IntNum(x))
    case BinExpr("+", BinExpr("+", BinExpr("**", w, IntNum(a)), BinExpr("*", BinExpr("*", IntNum(b), x), y)), BinExpr("**", z, IntNum(c))) =>
      if (w == x && y == z && a == 2 && b == 2 && c == 2) BinExpr("**", BinExpr("+", x, y), IntNum(2)) else node
    case BinExpr("+", BinExpr("-", BinExpr("**", w, IntNum(a)), BinExpr("*", BinExpr("*", IntNum(b), x), y)), BinExpr("**", z, IntNum(c))) =>
      if (w == x && y == z && a == 2 && b == 2 && c == 2) BinExpr("**", BinExpr("-", x, y), IntNum(2)) else node


    case BinExpr("*", a, BinExpr("/", IntNum(b),c)) => if(b == 1) BinExpr("/",a,c) else BinExpr("*", a, simplify(BinExpr("/", IntNum(b),c)))
    case BinExpr("+", Unary("-", Variable(x)),Variable(y)) => if(x == y) IntNum(0) else BinExpr("+", Unary("-", Variable(x)),Variable(y))
    case BinExpr("/", a, FloatNum(1)) => a
    case BinExpr("/", a, IntNum(b)) => if( b == 1) a else BinExpr("/", simplify(a), IntNum(b))
    case BinExpr("/", a, BinExpr("/",b,c)) => simplify(BinExpr("*",a,simplify(BinExpr("/", c,b))))
    case BinExpr("/", BinExpr(op, x, y), BinExpr(op1,x1,y1)) => if(op == op1 && (op == "*" || op == "+") && ((x == x1 && y == y1)||(x == y1 && y == x1)) ) IntNum(1) else BinExpr("/", simplify(BinExpr(op, x, y)), simplify(BinExpr(op1,x1,y1)))
    case BinExpr("/", x, y) => if (x == y) IntNum(1) else node
    case BinExpr("-", BinExpr("-", BinExpr("**", BinExpr("+", Variable(x), Variable(y)), IntNum(a)), BinExpr("**", Variable(z), IntNum(b))), BinExpr("*", BinExpr("*", IntNum(c), Variable(v)), Variable(w))) => if (x == z && x == v && y == w && a == b && a == c && a == 2) {
      BinExpr("**", Variable(y), IntNum(2))
    } else node
    case BinExpr("-", BinExpr("**", BinExpr("+", Variable(x), Variable(y)), IntNum(a)), BinExpr("**", BinExpr("-", Variable(z), Variable(v)), IntNum(b))) => if(x == z && y == v && a == b && a == 2) BinExpr("*", BinExpr("*", IntNum(4), Variable(x)), Variable(y)) else node

    case BinExpr("and", _, FalseConst()) => FalseConst()
    case BinExpr("and", FalseConst(), _) => FalseConst()
    case BinExpr("or", _, TrueConst()) => TrueConst()
    case BinExpr("or", TrueConst(), _) => TrueConst()


    case BinExpr("+", ElemList(list1), ElemList(list2)) => ElemList(list1 ++ list2)
    case BinExpr("+", Tuple(list1), Tuple(list2)) => Tuple(list1 ++ list2)

    case Assignment(x, y) => if (x != y) Assignment(x, simplify(y)) else EmptyInstruction()
    case WhileInstr(cond, body) => if(simplify(cond)==TrueConst()) WhileInstr(cond, simplify(body)) else EmptyInstruction()

    case IfElseInstr(cond, left, right) => {
      if (cond == TrueConst()) simplify(left)
      else if(cond == FalseConst()) simplify(right)
      else IfElseInstr(cond, simplify(left), simplify(right))
    }

    case IfElseExpr(cond, left, right) => {
      if (cond == TrueConst()) simplify(left)
      else if(cond == FalseConst()) simplify(right)
      else IfElseExpr(cond, simplify(left), simplify(right))
    }

    case BinExpr("or", x,FalseConst()) => x
    case BinExpr("and", x,TrueConst()) => x
    case BinExpr("==", x, y) => if(x==y) TrueConst() else BinExpr("==", x,y)
    case BinExpr(">=", x,y) => if(x==y) TrueConst() else BinExpr(">=", x,y)
    case BinExpr("<=", x,y) => if(x==y) TrueConst() else BinExpr("<=", x,y)
    case BinExpr("!=", x,y) => if(x==y) FalseConst() else BinExpr("!=", x,y)
    case BinExpr("<", x,y) => if(x==y) FalseConst() else BinExpr("<", x,y)
    case BinExpr(">", x,y) => if(x==y) FalseConst() else BinExpr(">", x,y)
    case BinExpr("or", x ,y) => if(x==y) x else BinExpr("or", x,y)
    case BinExpr("and", x,y) => if(x==y) x else BinExpr("and", x,y)
    case BinExpr(op, BinExpr(o1, a1, a2), BinExpr(o2, b1, b2)) => {
      val s1 = simplify(BinExpr(o1, a1, a2))
      val s2 = simplify(BinExpr(o2, b1, b2))
      if (s1 != BinExpr(o1, a1, a2) && s2 != BinExpr(o2, b1, b2)) simplify(BinExpr(op, s1, s2)) else BinExpr(op, s1, s2)
    }
    case BinExpr(op, BinExpr(o1, a1, a2), x) => {
      val s1 = simplify(BinExpr(o1, a1, a2))
      if (s1 != BinExpr(o1, a1, a2)) simplify(BinExpr(op, s1, x)) else BinExpr(op, s1, x)
    }
    case BinExpr(op, x, BinExpr(o1, a1, a2)) => {
      val s1 = simplify(BinExpr(o1, a1, a2))
      if (s1 != BinExpr(o1, a1, a2)) simplify(BinExpr(op, x, s1)) else BinExpr(op, x, s1)
    }

    case BinExpr(op, Unary(o1, a1), Unary(o2, b1)) => {
      val s1 = simplify(Unary(o1, a1))
      val s2 = simplify(Unary(o2, b1))
      if (s1 != Unary(o1, a1) && s2 != Unary(o2, b1)) simplify(BinExpr(op, s1, s2)) else BinExpr(op, s1, s2)
    }
    case BinExpr(op, x, Unary(o2, b1)) => {
      val s2 = simplify(Unary(o2, b1))
      if (s2 != Unary(o2, b1)) simplify(BinExpr(op, x, s2)) else BinExpr(op, x, s2)
    }
    case BinExpr(op, Unary(o1, a1), x) => {
      val s1 = simplify(Unary(o1, a1))
      if (s1 != Unary(o1, a1)) simplify(BinExpr(op, s1, x)) else BinExpr(op, s1, x)
    }
//    case NodeList(list: List[_]) =>
//      _ match {
//        case Assignment => {
//          var result = List[Assignment]()
//          var dict = Map(list(0).left -> list(0).right)
//          list.map(x => dict += x.key -> x.value)
//        }
//      }
    case NodeList(List(EmptyInstruction())) => EmptyInstruction()
    case(NodeList(a)) => {
      if (a(0).isInstanceOf[Assignment]) {
        a(0) match {
          case as: Assignment => {
            var dict = Map(as.left -> as.right)
            a.map(x => {
              x match {
                case as2: Assignment => dict += as2.left -> simplify(as2.right)
                case _ => x
              }
            })
            val res = ListBuffer[Assignment]()
            dict.foreach(keyVal => if (keyVal._1 != keyVal._2) res += Assignment(keyVal._1, keyVal._2))
            if (res.size > 0) NodeList(res.toList) else EmptyInstruction()
          }
          case _ => if(a.size == 1) simplify(a.head) else NodeList(a.map(simplify))
        }
      }
      else {
        if(a.size == 1) simplify(a.head) else NodeList(a.map(simplify))
      }
    }

    case _ => node
  }


//  def simplify(node: BinExpr ) =
//  def simplify(node: Node) = node
//  case class BinExpr(op: String, left: Node, right: Node) extends Node

}
